import { combineReducers } from 'redux';
// import { reducer as formReducer } from 'redux-form';
import GMEReducer from './reducer_gme';

const rootReducer = combineReducers({
  GME: GMEReducer
});

export default rootReducer;