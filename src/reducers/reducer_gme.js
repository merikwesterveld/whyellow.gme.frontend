import { LOGIN, 
        UPDATE_PROFILE, 
        LOGOUT, 
        FETCH_MATCHES, 
        LIKE, REGISTER, 
        FETCH_FAVORITES 
    } from '../actions/index';

export default function (state, action) {
    if (state == null) {
        state = {};
    }

    switch (action.type) {
        case LOGIN:
            localStorage.clear();
            localStorage.setItem("profile", JSON.stringify(action.payload["profile"]));
            localStorage.setItem("accessToken", action.payload["accessToken"]);
            state.profile = action.payload["profile"];
            state.accessToken = action.payload["accessToken"];
            return state;
        case REGISTER:
            localStorage.clear();
            localStorage.setItem("profile", JSON.stringify(action.payload["profile"]));
            localStorage.setItem("accessToken", action.payload["accessToken"]);
            state.profile = action.payload["profile"];
            state.accessToken = action.payload["accessToken"];
            return state;
        case UPDATE_PROFILE:
            state.amountOfCars = action.payload;
            if (state.amountOfCars == null) {
                state.amountOfCars = 0;
            }
            state.profile = action.values;
            localStorage.setItem("profile", JSON.stringify(action.values));
            return state;
        case LOGOUT:
            state = undefined;
            return state;
        case FETCH_MATCHES:
            state.noMatches = false;
            state.currentMatch = action.payload[0];
            state.nextMatch = action.payload[1];
            state.previousMatch = undefined;
            return state;
        case LIKE:
            var oldNext = state.nextMatch;
            var previous = state.currentMatch;

            state.nextMatch = action.payload[1];
            state.currentMatch = action.payload[0];
            state.previousMatch = previous;

            if (action.payload[0] == null) {
                state.noMatches = true;
            }
            return state;
        case FETCH_FAVORITES:
            state.favorites = action.payload;
        return state;
    }
    return state;
}