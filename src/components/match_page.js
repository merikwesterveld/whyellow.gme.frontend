import Inferno from 'inferno';
import Component from 'inferno-component';
import { connect } from 'inferno-redux';
import { bindActionCreators } from "redux";
import { fetchMatches, like, dislike } from '../actions';
import Forbidden from './forbidden_page';
import img from './logo-whyellow.png';

var BiggerTitle = { "font-size": "1.5vw" }
var centerScreen = { "min-height": "100%", "min-height": "70vh", "display": "flex", "align-items": "center", "position": "relative", "margin-left": "8%" }
var leftCard = { "margin-bottom": "80px", "box-shadow": "8px 8px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)", "width": "16%", "flex-wrap": "wrap", "flex": "initial", "font-size": "11px" }
var middleCard = { "margin-top": "30px", "box-shadow": "8px 8px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)", "width": "80vw", "flex-wrap": "wrap", "flex": "initial", "margin-left": "50px", "margin-right": "50px", "font-size": "13px" }
var rightCard = { "margin-bottom": "80px", "box-shadow": "20px 30px 24px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)", "width": "16%", "flex-wrap": "wrap", "flex": "initial", "font-size": "11px" }
var smallerButton = { "font-size": "10px" }
var smallerTitle = { "font-size": "15px" }
var buttonsStyle = { "margin-left": "45px", "margin-right": "10px" }
var buttonsStyleSmall = { "align-content": "center", "margin-left": "6%", "margin-right": "5%" }
var buttonsStyleSmallSC = { "align-content": "center", "margin-left": "1vw", "margin-right": "1vw" }
var buttonStyleMargin = { "margin": "20px", "font-size": "20px" }
var center = { "text-align": "center" }
var middleCardSC = { "margin-top": "30px", "box-shadow": "8px 8px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)", "width": "80vw", "flex-wrap": "wrap", "flex": "initial", "margin-left": "10vw", "margin-right": "15vw", "font-size": "13px" }
var scTitle = {"font-size": "6vw"}
var scText = { "font-size": "3vw"}

var profile = null;
var accessToken = null;
var match = null;
var first = true;
var smallScreen = false;

var currentMatch = null;
var previousMatch = null;
var nextMatch = null;

class MatchPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {}, currentMatch: { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } },
            nextMatch: { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } },
            previousMatch: { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } }
        };

        this.move = this.move.bind(this);
        this.fillCards = this.fillCards.bind(this);
        this.likeMatch = this.likeMatch.bind(this);
        this.moveCards = this.moveCards.bind(this);
        this.goBack = this.goBack.bind(this);
        this.dislikeMatch = this.dislikeMatch.bind(this);
        this.onResize = this.onResize.bind(this);
    }

    componentDidMount() {
        var width = window.innerWidth;
        this.setState({ width });

        this.props.fetchMatches(JSON.parse(localStorage.getItem("profile")), this.fillCards);
        if (width > 1200) {
            this.moveCards();
        }

    }

    goBack() {
        this.context.router.push(`/user/${profile.id}`);
    }

    fillCards() {
        currentMatch = this.props.GME.currentMatch;
        nextMatch = this.props.GME.nextMatch;
        previousMatch = this.props.GME.previousMatch;

        if (currentMatch == null) {
            currentMatch = { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } };
        }
        if (nextMatch == null) {
            nextMatch = { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } };
        }
        if (previousMatch == null) {
            previousMatch = { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } };
        }

        if (this.props.GME.noMatches) {
            currentMatch = { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } };
            nextMatch = { matchCase: { name: "", attributes: { Price: "", Color: "", Type: "", Doors: "" } } };
        }

        this.setState({ currentMatch, nextMatch, previousMatch });

        document.getElementsByClassName("card-text")[0].innerHTML = `<ul><li>Price: ${this.state.nextMatch.matchCase.attributes.Price}</li>` +
            `<li>Color: ${this.state.nextMatch.matchCase.attributes.Color}</li><li>Doors: ${this.state.nextMatch.matchCase.attributes.Doors}</li>` +
            `<li>Type: ${this.state.nextMatch.matchCase.attributes.Type}</li></ul>`
        document.getElementsByClassName("card-title")[0].innerHTML = `${this.state.nextMatch.matchCase.name}`

        if (this.state.width > 1200) {
            document.getElementsByClassName("card-text")[1].innerHTML = `<ul><li>Price: ${this.state.previousMatch.matchCase.attributes.Price}</li>` +
                `<li>Color: ${this.state.previousMatch.matchCase.attributes.Color}</li><li>Doors: ${this.state.previousMatch.matchCase.attributes.Doors}</li>` +
                `<li>Type: ${this.state.previousMatch.matchCase.attributes.Type}</li></ul>`

            document.getElementsByClassName("card-text")[2].innerHTML = `<ul><li>Price: ${this.state.currentMatch.matchCase.attributes.Price}</li>` +
                `<li>Color: ${this.state.currentMatch.matchCase.attributes.Color}</li><li>Doors: ${this.state.currentMatch.matchCase.attributes.Doors}</li>` +
                `<li>Type: ${this.state.currentMatch.matchCase.attributes.Type}</li></ul>`

            document.getElementsByClassName("card-title")[1].innerHTML = `${this.state.previousMatch.matchCase.name}`
            document.getElementsByClassName("card-title")[2].innerHTML = `${this.state.currentMatch.matchCase.name}`
        }
    }

    likeMatch() {
        if(this.state.width < 1200){
            document.getElementsByClassName("card-text")[0].innerHTML = '<i class="fa fa-spinner fa-spin" style="font-size:10vw"></i>'
        }
        this.props.like(profile.id, this.props.GME.currentMatch.matchCase.id, this.fillCards);
    }

    dislikeMatch() {
        if(this.state.width < 1200){
            document.getElementsByClassName("card-text")[0].innerHTML = '<i class="fa fa-spinner fa-spin" style="font-size:10vw"></i>'
        }
        this.props.dislike(profile.id, this.props.GME.currentMatch.matchCase.id, this.fillCards);
    }

    move(event) {
        console.log(JSON.stringify(event.target));
        if (!first && event.target.id === "like") {
            this.likeMatch();
        } else if (!first && event.target.id === "dislike") {
            this.dislikeMatch();
        }
        this.moveCards();
    }

    onResize(event) {
        var width = window.innerWidth;
        this.setState({ width });
        if (width > 1200) {
            if (smallScreen) {
                window.location.reload();
                smallScreen = false;
            }
        } else {
            smallScreen = true;
        }
    }

    moveCards() {
        first = false;
        document.getElementsByClassName("card-text")[0].innerHTML = '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'

        if (this.state.width > 1200) {
            document.getElementsByClassName("card-text")[1].innerHTML = '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'
            document.getElementsByClassName("card-text")[2].innerHTML = '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'
        }

        document.getElementsByClassName("card-title")[0].innerHTML = ''

        if (this.state.width > 1200) {
            document.getElementsByClassName("card-title")[1].innerHTML = ''
            document.getElementsByClassName("card-title")[2].innerHTML = ''
        }

        var leftCard = document.getElementById("left-card");
        var middleCard = document.getElementById("middle-card");
        var rightCard = document.getElementById("right-card");

        var leftInterval = setInterval(frame, 12);
        var leftPos = 0;
        var leftPos2 = 1250;

        var rightInterval = setInterval(frameRight, 6);
        var rightPos = 0;
        var rightScale = 1;
        var rightWidth = 16;
        var rightTop = 0;

        var middleInterval = setInterval(middleFrame, 10);
        var middlePos = 0;
        var middleScale = 1;
        var middleWidth = 40;
        var middleTop = 0;

        var int = 0;

        function frame() {
            if (leftPos <= -500) {
                if (leftPos2 <= 700) {
                    clearInterval(leftInterval);
                    leftPos = 0;
                    leftPos2 = 1250;
                } else {
                    leftPos2 = leftPos2 - 20;
                    leftCard.style.left = leftPos2 + 'px';
                }

            } else {
                leftPos = leftPos - 20;
                leftCard.style.left = leftPos + 'px';
            }
        }

        function frameRight() {
            if (rightPos <= -205) {
                clearInterval(rightInterval);
            } else {
                int++;
                rightPos = rightPos - 3.5;
                rightScale = rightScale + 0.00495;
                rightWidth = rightWidth + 0.175;
                rightTop = rightTop + 0.75;
                rightCard.style.left = rightPos + 'px';
                rightCard.style.transform = 'scale(' + rightScale + ')';
                rightCard.style.width = rightWidth + '%';
                rightCard.style.top = rightTop + 'px';
            }
        }

        function middleFrame() {
            if (middlePos <= -255) {
                clearInterval(middleInterval);
            } else {
                middleScale = middleScale - 0.0045;
                middlePos = middlePos - 7;
                middleWidth = middleWidth - 0.55;
                middleTop = middleTop - 1.5;
                middleCard.style.left = middlePos + 'px';
                middleCard.style.transform = 'scale(' + middleScale + ')';
                middleCard.style.width = middleWidth + '%';
                middleCard.style.top = middleTop + 'px';
            }
        }
    }

    render() {
        window.onresize = this.onResize.bind(this);

        if (this.state.width < 1200) {
            console.log(JSON.stringify(this.state.currentMatch));
            return (
                <div className="container-non-responsive">
                    <button style={buttonStyleMargin} type="submit" className="btn btn-danger" onClick={this.goBack.bind(this)}>Back</button>
                    <div className="card" style="width: 20rem;" id="middle-card" style={middleCardSC}>
                        <img className="card-img-top" src={img} alt="Card image cap" />
                        <div className="card-body">
                            <h4 style={scTitle} className="card-title">{this.state.currentMatch.matchCase.name}</h4>
                            <p className="card-text" style={scText}>
                                <ul>
                                    <li>Price: {this.state.currentMatch.matchCase.attributes.Price}</li>
                                    <li>Color: {this.state.currentMatch.matchCase.attributes.Color}</li>
                                    <li>Doors: {this.state.currentMatch.matchCase.attributes.Doors}</li>
                                    <li>Type: {this.state.currentMatch.matchCase.attributes.Type}</li>
                                </ul>
                            </p>
                            <div style={center}>
                                <a href="#" id="middle-card-button" style={buttonsStyleSmallSC} className="btn btn-primary"><i onClick={this.likeMatch.bind(this)}  class="fa fa-heart" style="font-size:3.5vw"></i></a>
                                <a href="#" id="middle-card-button" style={buttonsStyleSmallSC} className="btn btn-primary"><i onClick={this.dislikeMatch.bind(this)}  class="fa fa-times" style="font-size:3.5vw"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        profile = JSON.parse(localStorage.getItem("profile"));
        accessToken = localStorage.getItem("accessToken");
        if (profile == null || accessToken == null || this.props.params.userID != profile.id) {
            return <Forbidden />
        } else {
            return (
                <div className="container-non-responsive">
                    <button style={buttonStyleMargin} type="submit" className="btn btn-danger" onClick={this.goBack.bind(this)}>Back</button>
                    <div className="container">
                        <div className="card-deck" style={centerScreen}>
                            <div className="card" style="width: 20rem;" id="left-card" style={leftCard}>
                                <img className="card-img-top" src={img} alt="Card image cap" />
                                <div className="card-body">
                                    <h4 style={smallerTitle} className="card-title">{this.state.nextMatch.matchCase.name}</h4>
                                    <p className="card-text">
                                        <ul>
                                            <li>Price: {this.state.nextMatch.matchCase.attributes.Price}</li>
                                            <li>Color: {this.state.nextMatch.matchCase.attributes.Color}</li>
                                            <li>Doors: {this.state.nextMatch.matchCase.attributes.Doors}</li>
                                            <li>Type: {this.state.nextMatch.matchCase.attributes.Type}</li>
                                        </ul>
                                    </p>
                                    <a href="#" id="left-card-button" style={buttonsStyleSmall} className="btn btn-primary"><i class="fa fa-heart" style="font-size:13px"></i></a>
                                    <a href="#" id="left-card-button" style={buttonsStyleSmall} className="btn btn-primary"><i class="fa fa-times" style="font-size:13px"></i></a>
                                </div>
                            </div>
                            <div className="card" style="width: 20rem;" id="middle-card" style={middleCard}>
                                <img className="card-img-top" src={img} alt="Card image cap" />
                                <div className="card-body">
                                    <h4 style={smallerTitle} className="card-title">{this.state.previousMatch.matchCase.name}</h4>
                                    <p className="card-text">
                                        <ul>
                                            <li>Price: {this.state.previousMatch.matchCase.attributes.Price}</li>
                                            <li>Color: {this.state.previousMatch.matchCase.attributes.Color}</li>
                                            <li>Doors: {this.state.previousMatch.matchCase.attributes.Doors}</li>
                                            <li>Type: {this.state.previousMatch.matchCase.attributes.Type}</li>
                                        </ul>
                                    </p>
                                    <a href="#" id="middle-card-button" style={buttonsStyleSmall} className="btn btn-primary"><i class="fa fa-heart" style="font-size:13px"></i></a>
                                    <a href="#" id="middle-card-button" style={buttonsStyleSmall} className="btn btn-primary"><i class="fa fa-times" style="font-size:13px"></i></a>
                                </div>
                            </div>
                            <div className="card" style="width: 20rem;" id="right-card" style={rightCard}>
                                <img className="card-img-top" src={img} alt="Card image cap" />
                                <div className="card-body">
                                    <h4 style={BiggerTitle} className="card-title">{this.state.currentMatch.matchCase.name}</h4>
                                    <p className="card-text">
                                        <ul>
                                            <li>Price: {this.state.currentMatch.matchCase.attributes.Price}</li>
                                            <li>Color: {this.state.currentMatch.matchCase.attributes.Color}</li>
                                            <li>Doors: {this.state.currentMatch.matchCase.attributes.Doors}</li>
                                            <li>Type: {this.state.currentMatch.matchCase.attributes.Type}</li>
                                        </ul>
                                    </p>
                                    <a href="#" id="right-card-button" style={buttonsStyle} className="btn btn-primary"> <i onClick={this.move.bind(this)} class="fa fa-heart" id="like" style="font-size:20px"></i> </a>
                                    <a href="#" id="right-card-button" style={buttonsStyle} className="btn btn-primary"> <i onClick={this.move.bind(this)} class="fa fa-times" id="dislike" style="font-size:20px"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        GME: state.GME
    };
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchMatches, like, dislike }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MatchPage);