import Inferno from 'inferno';
import Component from 'inferno-component';

export default class NotFound extends Component {
    render() {
        return (
            <div className="container">
                <h2>404 not found.</h2>
                <h6><em>We are sorry, but the page you are looking is no where to be found.</em></h6>
            </div>
        );
    }
}