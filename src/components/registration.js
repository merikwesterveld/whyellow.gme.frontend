import Inferno from 'inferno';
import Component from 'inferno-component';
import { connect } from 'inferno-redux';
import { register } from '../actions';
import { bindActionCreators } from "redux";

import img from './logo-whyellow.png'

const ROOT_URL = `http://localhost:62775/api/`;

var loginStyle = { "text-align": "center", "margin-top": "10px", "max-width": "340px" }
var logoStyle = { "text-align": "center", "max-width": "300px", "max-height": "auto", "display": "block", "margin": "auto" }
var formStyle = { "text-align": "left", "display": "inline-block", "width": "100%" }
var buttonStyle = { "display": "inline-block", "width": "100%" }
var hidden = { "display": "none" }
var show = { "display": "block" }
var pageLayout = { "margin-top": "75px" }

class Registration extends Component {
    constructor(props) {
        super(props);

        this.state = { fields: {} };
        this.register = this.register.bind(this);
        this.goToUserPage = this.goToUserPage.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    register(event){
        event.preventDefault();
        var fields = this.state.fields;
        var values = {
            email: fields["email"],
            password: fields["password"]
        }
        this.props.register(values, this.goToUserPage);
    }

    handleChange(field, event) {
        if (event.target.value.trim() === "") {
            event.target.className = "form-control border border-danger";
        } else {
            event.target.className = "form-control";
        }
        var fields = this.state.fields;
        fields[field] = event.target.value;
        this.setState({ fields });
    }

    goToUserPage(err) {
        if (this.props.GME.profile == null || this.props.GME.accessToken == null || err !== undefined) {
            document.getElementById("email").className = "form-control border border-danger";
            document.getElementById("password").className = "form-control border border-danger";
            document.getElementById("error").style = { show };
        } else {
            this.context.router.push(`/user/${this.props.GME.profile.id}`);
        }
    }

    render() {
        return (
            <div style={pageLayout}>
            <div>
                <img style={logoStyle} src={img} />
            </div>
            <div style={loginStyle} className="container form-group">
                <h3>Register</h3>
                <form style={formStyle} name="loginForm" onSubmit={this.register.bind(this)}>
                    <div className="form-group">
                        <label for="emailInput">Email</label>
                        <input className="form-control" placeholder="Email" name="email" id="email" value={this.state.fields["email"]} onInput={this.handleChange.bind(this, "email")} />
                    </div>
                    <div className="form-group">
                        <label for="passwordInput">Password</label>
                        <input className="form-control" placeholder="Password" type="password" name="password" id="password" value={this.state.fields["password"]} onInput={this.handleChange.bind(this, "password")} />
                    </div>
                    <p id="error" style={hidden} className="text-danger">The email is already in use or you inserted a wrong email..</p>
                    <button style={buttonStyle} type="submit" className="btn btn-primary">Register</button>
                </form>
            </div>
        </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        GME: state.GME
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ register }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration);