import Inferno from 'inferno';
import Component from 'inferno-component';
import { connect } from 'inferno-redux';
import { bindActionCreators } from "redux";
import { updateProfile, logout } from '../actions';
import Forbidden from './forbidden_page';

var icon = { "font-size": "150px" }
var profileStyle = { "text-align": "center", "margin-top": "75px", "max-width": "340px" }
var formStyle = { "text-align": "left", "display": "inline-block", "width": "100%" }
var buttonStyle = { "display": "inline-block", "width": "100%" }
var buttonStyleMargin = { "display": "inline-block", "width": "100%", "margin-top": "10px" }

var profile = null;
var accessToken = null;

var priceInput = "";
var colorInput = "";
var doorsInput = "";

class UserPage extends Component {
    constructor(props) {
        super(props);

        this.state = { fields: {} };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.amountOfCars = this.amountOfCars.bind(this);
        this.searchForCars = this.searchForCars.bind(this);
        this.logoutMethod = this.logoutMethod.bind(this);
        this.goToFavoritesPage = this.goToFavoritesPage.bind(this);

        this.profile = this.props.GME.profile;
        this.accessToken = this.props.GME.accessToken;
    }

    componentDidMount() {
        console.log("PROFILE: " + JSON.stringify(profile));
        if (document.getElementById("amountOfCars") !== null) {
            document.getElementById("amountOfCars").innerHTML = `Amount of cars found:  <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>`
            this.props.updateProfile(profile, this.amountOfCars);
        }
    }

    goToFavoritesPage() {
        this.context.router.push(`/favorites/${profile.id}`); 
    }

    searchForCars() {
        this.context.router.push(`/match/${profile.id}`);
    }

    logoutMethod() {
        localStorage.clear();
        this.context.router.push(`/`);
    }

    amountOfCars() {
        if (this.props.GME.amountOfCars !== null) {
            document.getElementById("amountOfCars").innerHTML = `Amount of cars found: ${this.props.GME.amountOfCars}`
        }
        else {
            document.getElementById("amountOfCars").innerHTML = `Amount of cars found: 0`
        }
    }

    handleChange(field, event) {
        if (event.target.value.trim() === "") {
            event.target.className = "form-control border border-danger";
        } else {
            event.target.className = "form-control";
        }
        var fields = this.state.fields;
        fields[field] = event.target.value;
        this.setState({ fields });
    }

    handleSubmit(event) {
        event.preventDefault();
        document.getElementById("amountOfCars").innerHTML = `Amount of cars found:  <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>`
        var fields = this.state.fields;
        profile.attributes.Price = fields["price"];
        profile.attributes.Color = fields["color"];
        profile.attributes.Doors = fields["doors"];
        this.props.updateProfile(profile, this.amountOfCars);
    }

    render() {
        profile = JSON.parse(localStorage.getItem("profile"));
        accessToken = localStorage.getItem("accessToken");
        if (profile == null || accessToken == null || this.props.params.userID != profile.id) {
            return <Forbidden />
        } else {
            doorsInput = profile.attributes.Doors;
            priceInput = profile.attributes.Price;
            colorInput = profile.attributes.Color;
            console.log(JSON.stringify(profile));
            console.log("doorsInput: " + doorsInput + " priceInput: " + priceInput + " colorInput: " + colorInput)
            return (
                <div style={profileStyle} className="container form-group">
                    <div className="row">
                        <div className="col-lg-6">
                            <i style={icon} class="fa fa-user-circle" aria-hidden="true"></i>
                        </div>
                        <div className="col-lg-6">
                            <button style={buttonStyleMargin} type="submit" className="btn btn-primary" onClick={this.goToFavoritesPage.bind(this)}>Favorites</button>
                            <button style={buttonStyleMargin} type="submit" className="btn btn-primary" onClick={this.searchForCars.bind(this)}>Search for cars</button>
                            <button style={buttonStyleMargin} type="submit" className="btn btn-danger" onClick={this.logoutMethod.bind(this)}>Logout</button>
                        </div>
                    </div>
                    <br />
                    <p id="amountOfCars">Amount of cars found:</p>

                    <form style={formStyle} name="loginForm" onSubmit={this.handleSubmit.bind(this)}>
                        <div className="form-group">
                            <p>Price</p>
                            <input type="number" className="form-control" placeholder="Price" name="price" id="price" defaultValue={priceInput} value={this.state.fields["price"]} onInput={this.handleChange.bind(this, "price")} />
                        </div>
                        <div className="form-group">
                            <p>Color</p>
                            <select className="form-control" placeholder="Color" name="color" defaultValue={colorInput} id="color" value={this.state.fields["color"]} onInput={this.handleChange.bind(this, "color")} >
                                <option value="blue">Blue</option>
                                <option value="yellow">Yellow</option>
                                <option value="black">Black</option>
                                <option value="green">Green</option>
                                <option value="red">Red</option>
                                <option value="white">White</option>
                                <option value="grey">Grey</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <p>Amount of doors</p>
                            <input type="number" className="form-control" placeholder="Doors" name="doors" id="doors" defaultValue={doorsInput} value={this.state.fields["doors"]} onInput={this.handleChange.bind(this, "doors")} />
                        </div>
                        <button style={buttonStyle} type="submit" className="btn btn-primary">Update</button>
                    </form>
                </div>
            );
        }
    }
}


function mapStateToProps(state) {
    return {
        GME: state.GME
    };
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ updateProfile, logout }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);