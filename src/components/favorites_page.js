import Inferno from 'inferno';
import Component from 'inferno-component';
import { connect } from 'inferno-redux';
import { bindActionCreators } from "redux";
import { fetch_favorites } from '../actions';
import Forbidden from './forbidden_page';
import _ from 'lodash';
import img from './logo-whyellow.png';

var marginCol = { "margin-top": "10px", "margin-bottom": "20px" }
var center = { "text-align": "left"}
var buttonStyleMargin = { "display": "inline-block", "margin-top": "10px" }

class FavoritesPage extends Component {
    constructor(props) {
        super(props);

        this.showCards = this.showCards.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    componentDidMount() {
        this.props.fetch_favorites(JSON.parse(localStorage.getItem("profile")), this.showCards)
    }

    goBack() {
        this.context.router.push(`/user/${JSON.parse(localStorage.getItem("profile")).id}`); 
    }

    showCards() {
        this.setState(this.props.GME.favorites);
    }

    renderFavorites() {
        var favs = _.uniqBy(this.props.GME.favorites, 'id');
        console.log(JSON.stringify(favs));
        return _.map(favs, fav => {
            return (
                <div style={marginCol} className="col-lg-4 col-sm-6">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src={img} alt="Card image cap" />
                        <div class="card-body">
                            <h4 class="card-title">{fav.name}</h4>
                            <p class="card-text">
                                <ul>
                                    <li>Price: {fav.attributes.Price}</li>
                                    <li>Color: {fav.attributes.Color}</li>
                                    <li>Doors: {fav.attributes.Doors}</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4">
                            <button style={buttonStyleMargin} onClick={this.goBack.bind(this)} className="btn btn-danger">Back</button>
                        </div>
                        <div className="col-lg-4">
                            <h1 style={center}>Your liked cases</h1>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {this.renderFavorites()}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        GME: state.GME
    };
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetch_favorites }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesPage);