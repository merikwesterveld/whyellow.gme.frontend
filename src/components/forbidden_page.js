import Inferno from 'inferno';
import Component from 'inferno-component';

export default class Forbidden extends Component {
    render() {
        return (
            <div className="container">
                <h2>403 forbidden.</h2>
                <h6><em>We are sorry, but the page you are looking for is only for authorized users.</em></h6>
            </div>
        );
    }
}