import axios from 'axios';

export const LOGIN = 'login';
export const UPDATE_PROFILE = 'update_profile';
export const LOGOUT = 'logout';
export const FETCH_MATCHES = 'fetch_matches';
export const LIKE = 'like';
export const REGISTER = 'register';
export const FETCH_FAVORITES = 'fetch_favorites';

const ROOT_URL = `http://localhost:62775/api/`;

export function login(values, callback) {
    const request = axios.post(`${ROOT_URL}profiles/login`, values);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: LOGIN, payload: data });
            callback();
        }).catch((err) => {
            callback(err);
        });
    }
}

export function updateProfile(values, callback) {
    var token = localStorage.getItem("accessToken");

    var config = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }

    const request = axios.put(`${ROOT_URL}profiles/40`, values, config);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: UPDATE_PROFILE, payload: data, values: values });
            callback();
        }).catch(() => {
            callback();
        });
    }
}

export function logout() {
    return (dispatch) => {
        dispatch({
            type: LOGOUT,
            payload: "undefined"
        })
    }
}

export function fetchMatches(values, callback) {
    var token = localStorage.getItem("accessToken");

    var config = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }

    const request = axios.get(`${ROOT_URL}matches/${values.id}/2/40`, config);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: FETCH_MATCHES, payload: data, values: values });
            callback();
        }).catch(() => {
            callback();
        });
    }
}

export function like(profileId, caseId, callback) {
    var token = localStorage.getItem("accessToken");
    var config = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }

    const request = axios.post(`${ROOT_URL}cases/${caseId}/Like/${profileId}/2/40`, null, config);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: LIKE, payload: data});
            callback();
        }).catch(() => {
            callback();
        });
    }
}

export function dislike(profileId, caseId, callback){
    var token = localStorage.getItem("accessToken");
    var config = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }

    const request = axios.post(`${ROOT_URL}cases/${caseId}/Dislike/${profileId}/2/40`, null, config);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: LIKE, payload: data});
            callback();
        }).catch(() => {
            callback();
        });
    }
}

export function register(values , callback) {
    const request = axios.post(`${ROOT_URL}profiles/register`, values);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: REGISTER, payload: data });
            callback();
        }).catch((err) => {
            callback(err);
        });
    }
}

export function fetch_favorites(values, callback) {
    var token = localStorage.getItem("accessToken");
    var config = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }

    const request = axios.get(`${ROOT_URL}cases/likedCases/${values.id}`, config);
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: FETCH_FAVORITES, payload: data});
            callback();
        }).catch(() => {
            callback();
        });
    }
}