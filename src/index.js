import { render } from 'inferno';
import Home from './components/home.js';
import UserPage from './components/user_page.js';
import NotFoundPage from './components/not_found_page.js';
import MatchPage from './components/match_page.js'
import FavoritesPage from './components/favorites_page.js';
import Registration from './components/registration.js'
import { Router, Link, Route, IndexRoute } from 'inferno-router';
import createBrowserHistory from 'history/createBrowserHistory';

var express = require("express");
var app = express();
app.set('port', process.env.PORT || 8080);

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'inferno-redux';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import reducers from './reducers';

const browserHistory = createBrowserHistory();
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

const routes = (
	<Provider store={createStoreWithMiddleware(reducers)}>
		<Router history={browserHistory}>
			<Route path="/user/:userID" component={UserPage} />
			<Route path="/match/:userID" component={MatchPage} />
			<Route path="/registration" component={Registration} />
			<Route path="/favorites/:userID" component={FavoritesPage} />
			<Route path="/" component={Home} />
			<Route path="*" component={NotFoundPage} />
		</Router>
	</Provider>
);

render(routes, document.getElementById('app'));
const port = process.env.PORT || 8080;
app.listen(port);