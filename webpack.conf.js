const webpack = require('webpack');

module.exports = {
	entry: './src/index.js',
	output: {
		path: __dirname + '/public',
		filename: 'bundle.js',
		publicPath: 'http://0.0.0.0:8080/'
	},
	module: {
		loaders: [
			{
				test: /\.js$/, loader: 'babel-loader'
			},
			{
				test: /\.(png|jpg|gif)$/, use: [
					{
						loader: 'url-loader',
						options: {
							limit: 8192
						}
					}
				]
			}]
	},
	devServer: {
		contentBase: './',
		port: 8080,
		noInfo: false,
		hot: true,
		inline: true,
		proxy: {
			'/': {
				bypass: function (req, res, proxyOptions) {
					return '/public/index.html';
				}
			}
		}
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	]
};